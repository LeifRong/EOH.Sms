## EOH.Sms SMS API 封装组件
该组件旨在简化与阿里云和腾讯云短信 API 的集成，提供了易于使用的方法来发送短信和处理短信发送状态。
通过封装底层的 API 调用，你可以更加专注于业务逻辑，而不需要过多关心底层细节。
EOH(Extended Office hours)延长办公时间

### 功能
1. 阿里云短信
- ✅ 短链管理
- ✅ 短信发送
- ✅ 发送查询
- ✅ 签名管理
- ✅ 模板管理
- ✅ 国际短信
- - [x] 标签管理
- - [x] 卡片短信
2. 腾讯云短信
- - [x] 暂时无环境，延后实现
##### 安装
使用 NuGet 包管理器安装：

##### 使用示例
初始化

```cs
var service = new ServiceCollection();
service.AddAlibabaSmsService((options) =>
{
    options.AccessKeyId = "AccessKeyId ";
    options.AccessKeySecret = "AccessKeySecret ";
});
```

发送短信

```cs
var res = await _AlibabaService.Execute(new SendSmsRequest
{
    Model = new SendSmsModel
    {
        PhoneNumbers = new List<string> { "18688864248" },
        SignName = "**",
        TemplateCode = "**",
        TemplateParam = "{\"code\":\"1111\"}"
    }
});

var res = _AlibabaService.Execute(new SendBatchSmsRequest
{
    Model = new SendBatchSmsModel
    {
        PhoneNumberJson = new List<string> { "18688864248" },
        SignNameJson = new List<string> { "签名" },
        TemplateCode = "SMS_***",
        TemplateParamJson = new List<string> { "{\"code\":\"1111\"}" }
    }
});
```


##### 贡献
欢迎贡献代码、报告问题和提供建议！请在 Gitee/GitHub 项目中提交 Issue 或 Pull Request。

##### 许可
本项目基于 MIT 许可进行分发和使用。详细信息请查阅 LICENSE 文件。

##### 作者
作者：荣少<br/>
邮箱：ligengrong@hotmail.com<br/>
QQ群：7405133

##### 🏅开源地址
[![Gitee](https://shields.io/badge/Gitee-https://gitee.com/LeifRong/EOH.Sms-green?logo=gitee&style=flat&logoColor=red)](https://gitee.com/LeifRong/EOH.Sms)  <br/>

### 🧧 赞赏作者

	你的赞赏就是我前进的动力🏃，
	如果EOH.Sms给你带来了不少便利，
	那就请作者喝杯咖啡☕吧。
![](https://gitee.com/LeifRong/EOH.Sms/raw/master/img/pay.jpg)