using Microsoft.Extensions.DependencyInjection;

using EOH.Sms;
using EOH.Sms.Alibaba;
using EOH.Sms.Alibaba.Req;

namespace Test
{
    [TestClass]
    public class AlibabaTest
    {
        static IService _AlibabaService;
        [ClassInitialize]
        public static void Initialize(TestContext context)
        {
            var service = new ServiceCollection();
            service.AddAlibabaSmsService((options) =>
            {
                options.AccessKeyId = "";
                options.AccessKeySecret = "";
            });
            _AlibabaService = service.BuildServiceProvider().GetService<AlibabaService>();
        }
        #region 发短信
        [TestMethod]
        public void SendSmsText()
        {
            var res = _AlibabaService.Execute(new SendSmsRequest
            {
                Model = new SendSmsModel
                {
                    PhoneNumbers = new List<string> { "18688864248" },
                    SignName = "**",
                    TemplateCode = "**",
                    TemplateParam = "{\"code\":\"1111\"}"
                }
            }).Result;
            Assert.AreEqual("OK", res.Code);
        }

        [TestMethod]
        public void SendBatchSmsText()
        {
            var res = _AlibabaService.Execute(new SendBatchSmsRequest
            {
                Model = new SendBatchSmsModel
                {
                    PhoneNumberJson = new List<string> { "18688864248" },
                    SignNameJson = new List<string> { "***" },
                    TemplateCode = "SMS_***",
                    TemplateParamJson = new List<string> { "{\"code\":\"1111\"}" }
                }
            }).Result;
            Assert.AreEqual("OK", res.Code);
        }
        #endregion

        #region 发送查询
        [TestMethod]
        public void QuerySendStatisticsText()
        {
            var res = _AlibabaService.Execute(new QuerySendStatisticsRequest
            {
                Model = new QuerySendStatisticsModel
                {
                    StartDate = "20230810",
                    EndDate = "20230812",
                }
            }).Result;
            Assert.AreEqual("OK", res.Code);
        }
        [TestMethod]
        public void QuerySendDetailsText()
        {
            var res = _AlibabaService.Execute(new QuerySendDetailsRequest
            {
                Model = new QuerySendDetailsModel
                {
                    PhoneNumber = "18688864248",
                    SendDate = "20230811",
                }
            }).Result;
            Assert.AreEqual("OK", res.Code);
        }
        #endregion

        #region 短链接管理
        [TestMethod]
        public void ShortUrlText()
        {
            string shortUrl = string.Empty;

            {
                var res = _AlibabaService.Execute(new AddShortUrlRequest
                {
                    Model = new AddShortUrlModel
                    {
                        SourceUrl = "https://www.baidu.com/",
                        ShortUrlName = "百度",
                        EffectiveDays = "90"
                    }
                }).Result;
                Assert.AreEqual("OK", res.Code);
                shortUrl = res?.Data?.ShortUrl;
            }
            {
                var res = _AlibabaService.Execute(new QueryShortUrlRequest
                {
                    Model = new QueryShortUrlModel
                    {
                        ShortUrl = shortUrl,
                    }
                }).Result;

                Assert.AreEqual("OK", res.Code);
            }
            {
                var res = _AlibabaService.Execute(new DeleteShortUrlRequest
                {
                    Model = new DeleteShortUrlModel
                    {
                        SourceUrl = "https://www.baidu.com/",
                    }
                }).Result;
                Assert.AreEqual("OK", res.Code);
            }
        }

        #endregion

        #region 签名管理
        [TestMethod]
        public void AddSmsSignText()
        {
            var res = _AlibabaService.Execute(new AddSmsSignRequest
            {
                Model = new AddSmsSignModel
                {
                    SignName = "测试",
                    Remark = "测试无需审核"
                }
            }).Result;
            Assert.AreEqual("OK", res.Code);
        }
        [TestMethod]
        public void QuerySmsSignText()
        {
            var res = _AlibabaService.Execute(new QuerySmsSignRequest
            {
                Model = new DeleteSmsSignModel
                {
                    SignName = "测试",
                }
            }).Result;
            Assert.AreEqual("OK", res.Code);
        }
        [TestMethod]
        public void QuerySmsSignListText()
        {
            var res = _AlibabaService.Execute(new QuerySmsSignListRequest
            {
                Model = new QuerySmsSignListModel { }
            }).Result;
            Assert.AreEqual("OK", res.Code);
        }
        [TestMethod]
        public void DeleteSmsSignText()
        {
            var res = _AlibabaService.Execute(new DeleteSmsSignRequest
            {
                Model = new DeleteSmsSignModel
                {
                    SignName = "测试",
                }
            }).Result;
            Assert.AreEqual("OK", res.Code);
        }
        #endregion

        #region 模板管理
        [TestMethod]
        public void AddSmsTemplateText() {
            var res = _AlibabaService.Execute(new AddSmsTemplateRequest
            {
                Model = new AddSmsTemplateModel
                {
                    TemplateName = "测试",
                    TemplateContent = "设备-${name}，告警级别：${AlarmLevelName},告警类容：${AlarmDescription}",
                    Remark = "用于物联网设置警告通知使用测试",
                }
            }).Result;
            Assert.AreEqual("OK", res.Code);
        }
        [TestMethod]
        public void DeleteSmsTemplateText() {
            var res = _AlibabaService.Execute(new DeleteSmsTemplateRequest
            {
                Model = new DeleteSmsTemplateModel
                {
                    TemplateCode = "SMS_462650261",
                }
            }).Result;
            Assert.AreEqual("OK", res.Code);
        }
        [TestMethod]
        public void ModifySmsTemplateText()
        {
            var res = _AlibabaService.Execute(new ModifySmsTemplateRequest
            {
                Model = new ModifySmsTemplateModel
                {
                    TemplateCode = "SMS_221732446",
                    TemplateName = "警告通知",
                    TemplateContent = "设备-${name}，告警级别：${AlarmLevelName},告警类容：${AlarmDescription}",
                    Remark = "用于物联网设置警告通知使用测试",
                }
            }).Result;
            Assert.AreEqual("OK", res.Code);
        }
        [TestMethod]
        public void QuerySmsTemplateListText()
        {
            var res = _AlibabaService.Execute(new QuerySmsTemplateListRequest
            {
                Model = new QuerySmsSignListModel { }
            }).Result;
            Assert.AreEqual("OK", res.Code);
        }
        [TestMethod]
        public void QuerySmsTemplateText()
        {
            var res = _AlibabaService.Execute(new QuerySmsTemplateRequest
            {
                Model = new DeleteSmsTemplateModel
                {
                    TemplateCode = "SMS_221732446",
                }
            }).Result;
            Assert.AreEqual("OK", res.Code);
        }
        #endregion

        #region 标签管理
        [TestMethod]
        public void ListTagResourcesText() {
            var res = _AlibabaService.Execute(new ListTagResourcesRequest
            {
                Model = new ListTagResourcesModel { }
            }).Result;
            Assert.AreEqual("OK", res.Code);
        }
        [TestMethod]
        public void TagResourcesText() {
            var res = _AlibabaService.Execute(new TagResourcesRequest
            {
                Model = new TagResourcesModel
                {
                    Tag = new List<Tag> { new Tag { Key = "测试", Value = "测试" } },
                    ResourceId = new List<string> { "SMS_221732446" }
                }
            }).Result;
            Assert.AreEqual("OK", res.Code);
        }
        [TestMethod]
        public void UntagResourcesText() {
            var res = _AlibabaService.Execute(new UntagResourcesRequest
            {
                Model = new UntagResourcesModel
                {
                    TagKey = new List<string> { "测试" },
                    ResourceId = new List<string> { "SMS_221732446" },
                }
            }).Result;
            Assert.AreEqual("OK", res.Code);
        }
        #endregion

        #region 卡片短信
        [TestMethod]
        public void GetOSSInfoForCardTemplateText() {

            var res = _AlibabaService.Execute(new GetOSSInfoForCardTemplateRequest { }).Result;
            Assert.AreEqual("OK", res.Code);
        }
        [TestMethod]
        public void GetMediaResourceIdText() { }
        [TestMethod]
        public void CreateCardSmsTemplateText() { }
        [TestMethod]
        public void QueryCardSmsTemplateText() { }
        [TestMethod]
        public void CheckMobilesCardSupportText() { }
        [TestMethod]
        public void QueryMobilesCardSupportText() { }
        [TestMethod]
        public void GetCardSmsLinkText() { }
        [TestMethod]
        public void QueryCardSmsTemplateReportText() { }
        [TestMethod]
        public void SendCardSmsText() { }
        [TestMethod]
        public void SendBatchCardSmsText() { }
        #endregion

        #region 国际短信
        [TestMethod]
        public void SmsConversionIntlText() {
            //暂时不太明白用处
            var res = _AlibabaService.Execute(new SmsConversionIntlRequest
            {
                Model = new SmsConversionIntlModel
                {
                    MessageId = "1349055900000",
                    Delivered = true,
                    ConversionTime = 11
                }
            }).Result;
            Assert.AreEqual("OK", res.Code);
        }
        [TestMethod]
        public void ConversionDataIntlText() {
            var res = _AlibabaService.Execute(new ConversionDataIntlRequest
            {
                Model = new ConversionDataIntlModel
                {
                    ReportTime = 11111,
                    ConversionRate = "0.53",
                }
            }).Result;
            Assert.AreEqual("OK", res.Code);
        }
        #endregion
    }
}