﻿using System.Net.Http;
using System.Text.Json;

namespace EOH.Sms.Tencent
{
    public abstract class TencentRequest<TResponse> : IRequest<TResponse>
    {
        public string Action { get; set; }
        public HttpMethod Method { get; set; } = HttpMethod.Get;
        public virtual TResponse ConvertResponse(string content)
        => JsonSerializer.Deserialize<TResponse>(content, new JsonSerializerOptions { PropertyNameCaseInsensitive = true });
    }
    public abstract class TencentRequest<TModel, TResponse> : TencentRequest<TResponse>, IRequest<TModel, TResponse>
    {
        public TModel Model { get; set; }
    }
}
