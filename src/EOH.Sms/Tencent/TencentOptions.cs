﻿namespace EOH.Sms.Tencent
{
    /// <summary>配置项</summary>
    public class TencentOptions
    {
        /// <summary>访问密钥ID</summary>
        public string AccessKeyId { get; set; }
        /// <summary>访问密钥</summary>
        public string AccessKeySecret { get; set; }
        /// <summary>应用ID</summary>
        public string AppId { get; set; }
        /// <summary>语言</summary>
        public string Language { get; set; } = "zh-CN";
        /// <summary>短信服务Url地域参数</summary>
        public string Region { get; set; } = "cn-shenzhen";
        /// <summary>短信服务Url</summary>
        public string BaseAddressUrl { get; set; } = "https://sms.tencentcloudapi.com";
        /// <summary>接口版本，默认：2017-05-25</summary>
        public string Version { get; set; } = "2017-03-12";
    }
}
