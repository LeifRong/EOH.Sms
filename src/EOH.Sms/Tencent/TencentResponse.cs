﻿using System.Collections.Generic;

namespace EOH.Sms.Tencent
{
    public class TencentResponse
    {
        public TencentResponseMsg Response { get; set; }
    }

    public class TencentResponseMsg {
        /// <summary>错误信息</summary>
        public TencentResponseErrorMsg Error { get; set; }
        /// <summary>请求ID</summary>
        public string RequestId { get; set; }
        /// <summary>总数量 DescribeInstancesStatus 接口定义的字段</summary>
        public int TotalCount { get; set; }
        /// <summary>DescribeInstancesStatus 接口定义的字段</summary>
        public IList<string> InstanceStatusSet { get; set; }
    }
    public class TencentResponseErrorMsg {
        /// <summary>请求状态码</summary>
        public string Code { get; set; }
        /// <summary>状态码的描述</summary>
        public string Message { get; set; }
    }
}
