﻿using System.Net.Http;

namespace EOH.Sms
{
    public interface IRequest<TResponse>
    {
        /// <summary>HttpMethod</summary>
        HttpMethod Method { get; set; }
        /// <summary>API参数返回转换函数</summary>
        /// <param name="content">API返回字符串</param>
        /// <returns></returns>
        TResponse ConvertResponse(string content);
    }
    public interface IRequest<TModel, TResponse> : IRequest<TResponse>
    {

        /// <summary>参数实体</summary>
        TModel Model { get; set; }

    }
}
