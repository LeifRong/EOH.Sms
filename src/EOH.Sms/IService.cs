﻿using System.Threading.Tasks;

namespace EOH.Sms
{
    public interface IService
    {
        Task<TResponse> Execute<TResponse>(IRequest<TResponse> request);
        /// <summary>
        /// 执行请求
        /// </summary>
        /// <typeparam name="TModel">请求参数实体</typeparam>
        /// <typeparam name="TResponse">返回参数实体</typeparam>
        /// <param name="request">请求实体</param>
        /// <param name="sourceLineNum">调用者的行号</param>
        /// <param name="sourceFilePath">调用者的文件路径</param>
        /// <param name="sourceMmemberName">调用者的方法名</param>
        /// <returns></returns>
        Task<TResponse> Execute<TModel, TResponse>(IRequest<TModel, TResponse> request);
    }
}
