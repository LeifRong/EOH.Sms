﻿namespace EOH.Sms.Alibaba
{
    public class AlibabaResponse
    {
        /// <summary>请求状态码</summary>
        public string Code { get; set; }
        /// <summary>状态码的描述</summary>
        public string Message { get; set; }
        /// <summary>请求ID</summary>
        public string RequestId { get; set; }
    }
}
