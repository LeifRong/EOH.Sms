﻿using System.Collections.Generic;

namespace EOH.Sms.Alibaba.Req
{
    /// <summary>查询卡片短信模板状态</summary>
    public class QueryCardSmsTemplateRequest : AlibabaRequest<QueryCardSmsTemplateModel, QueryCardSmsTemplateResponse>
    {
        public QueryCardSmsTemplateRequest()
        {
            Action = "QueryCardSmsTemplate";
        }
    }
    public class QueryCardSmsTemplateModel
    {
        /// <summary>短信模板CODE</summary>
        /// <remarks>必须是已添加、并通过审核的模板CODE</remarks>
        public string TemplateCode { get; set; }
    }
    public class QueryCardSmsTemplateResponse : AlibabaResponse {
        public QueryCardSmsTemplateData Data { get; set; }
    }
    public class QueryCardSmsTemplateData { 
        public IList<Templates> Templates { get; set; }
    }

    public class Templates
    {
        /// <summary>模板名称</summary>
        public string tmpName { get; set; }
        /// <summary>模板Code</summary>
        public string tmpCode { get; set; }
        /// <summary>模板审核状态。0：审核中，1：审核通过。2：审核未通过</summary>
        public string state { get; set; }
        /// <summary>手机厂商审核列表。（state：手机厂商审核状态。vendorCode：手机厂商信息。remark：手机厂商审核意见。）</summary>
        public string tmpOps { get; set; }
    }
}
