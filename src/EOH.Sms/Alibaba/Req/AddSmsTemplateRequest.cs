﻿namespace EOH.Sms.Alibaba.Req
{
    /// <summary>申请短信模板</summary>
    public class AddSmsTemplateRequest : AlibabaRequest<AddSmsTemplateModel, AddSmsSignResponseResponse>
    {
        public AddSmsTemplateRequest()
        {
            Action = "AddSmsTemplate";
        }
    }
    public class AddSmsTemplateModel
    {
        /// <summary>短信类型, 默认1</summary>
        /// <remarks>
        /// <para>0：验证码</para>
        /// <para>1：短信通知</para>
        /// <para>2：推广短信</para>
        /// <para>3：国际/港澳台消息</para>
        /// </remarks>
        public int TemplateType { get; set; } = 1;
        /// <summary>模板名称，长度不超过30个字符</summary>
        public string TemplateName { get; set; }
        /// <summary>模板内容，长度不超过500个字符</summary>
        /// <remarks>您正在申请手机注册，验证码为：${code}，5分钟内有效！</remarks>
        public string TemplateContent { get; set; }
        /// <summary>短信模板申请说明，是模板审核的参考信息之一。长度不超过100个字符</summary>
        public string Remark { get; set; }
    }
    public class AddSmsSignResponseResponse : AlibabaResponse {
        /// <summary>短信模板Code</summary>
        public string TemplateCode { get; set; }
    }
}
