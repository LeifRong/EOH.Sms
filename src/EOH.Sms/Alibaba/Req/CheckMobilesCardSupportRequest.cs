﻿using System.Collections.Generic;

namespace EOH.Sms.Alibaba.Req
{
    /// <summary>接收短信的手机号码列表</summary>
    public class CheckMobilesCardSupportRequest : AlibabaRequest<CheckMobilesCardSupportModel, CheckMobilesCardSupportResponse>
    {
        public CheckMobilesCardSupportRequest()
        {
            Action = "CheckMobilesCardSupport";
        }
    }
    public class CheckMobilesCardSupportModel
    {
        /// <summary>短信模板CODE</summary>
        public string TemplateCode { get; set; }
        /// <summary>接收短信的手机号码列表</summary>
        public IList<Mobiles> Mobiles { get; set; }
    }
    public class Mobiles { }

    public class CheckMobilesCardSupportResponse : AlibabaResponse
    {
        /// <summary>返回对象。</summary>
        public CheckMobilesCardSupportData Data { get; set; }
    }
    public class CheckMobilesCardSupportData
    {
        public IList<CheckMobilesCardSupportQueryResult> queryResult { get; set; }
    }
    public class CheckMobilesCardSupportQueryResult {
        /// <summary>手机号码</summary>
        public string mobile { get; set; }
        /// <summary>是否支持卡片短信能力</summary>
        public bool support { get; set; }
    }
}
