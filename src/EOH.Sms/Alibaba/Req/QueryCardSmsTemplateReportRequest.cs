﻿using System.Collections.Generic;

namespace EOH.Sms.Alibaba.Req
{
    /// <summary>查询卡片短信发送详情</summary>
    public class QueryCardSmsTemplateReportRequest : AlibabaRequest<QueryCardSmsTemplateReportModel, AlibabaResponse>
    {
        public QueryCardSmsTemplateReportRequest()
        {
            Action = "QueryCardSmsTemplateReport";
        }
    }
    public class QueryCardSmsTemplateReportModel
    {
        /// <summary>卡片短信对象,卡片短信模板Code</summary>
        public IList<string> TemplateCodes { get; set; }
        /// <summary>开始时间，格式为yyyy-MM-dd HH:mm:ss</summary>
        public string StartDate { get; set; }
        /// <summary>结束时间，格式为yyyy-MM-dd HH:mm:ss</summary>
        public string EndDate { get; set; }
    }
    public class QueryCardSmsTemplateReportResponse : AlibabaResponse
    {
        public QueryCardSmsTemplateReportData Data { get; set; }
    }
    public class QueryCardSmsTemplateReportData
    {
        public IList<Model> model { get; set; }
    }

    public class Model
    {
        /// <summary>模板Code</summary>
        public string tmpCode { get; set; }
        /// <summary>数据日期。格式为yyyy-MM-dd HH:mm:ss</summary>
        public string date { get; set; }
        /// <summary>短信解析回执成功数</summary>
        public int rptSuccessCount { get; set; }
        /// <summary>消息曝光数</summary>
        public int exposeUv { get; set; }
        /// <summary>消息点击数</summary>
        public int exposePv { get; set; }
        /// <summary>消息点击数</summary>
        public int clickUv { get; set; }
        /// <summary>消息点击次数</summary>
        public int clickPv { get; set; }
    }
}
