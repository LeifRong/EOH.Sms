﻿namespace EOH.Sms.Alibaba.Req
{
    /// <summary>查询手机是否支持卡片短信</summary>
    public class QueryMobilesCardSupportRequest : AlibabaRequest<CheckMobilesCardSupportModel, CheckMobilesCardSupportResponse>
    {
        public QueryMobilesCardSupportRequest()
        {
            Action = "QueryMobilesCardSupport";
        }
    }
}
