﻿using System.Collections.Generic;

namespace EOH.Sms.Alibaba.Req
{
    /// <summary>发送卡片短信</summary>
    public class SendCardSmsRequest : AlibabaRequest<SendCardSmsModel, SendCardSmsResponse>
    {
        public SendCardSmsRequest()
        {
            Action = "SendCardSms";
        }
    }
    public class SendCardSmsModel
    {
        /// <summary>卡片短信对象</summary>
        public IList<CardObjects> CardObjects { get; set; }
        /// <summary>短信签名名称</summary>
        public string SignName { get; set; }
        /// <summary>卡片短信模板Code</summary>
        public string CardTemplateCode { get; set; }
        /// <summary>回落文本短信的模板Code</summary>
        public string SmsTemplateCode { get; set; }
        /// <summary>上行短信扩展码。上行短信，指发送给通信服务提供商的短信，用于定制某种服务、完成查询，或是办理某种业务等，需要收费的，按运营商普通短信资费进行扣费</summary>
        public string SmsUpExtendCode { get; set; }
        /// <summary>回落类型</summary>
        /// <remarks>
        /// <para>SMS：不支持卡片短信的号码，回落文本短</para>
        /// <para>DIGITALSMS：不支持卡片短信的号码，回落数字短信</para>
        /// <para>NONE：不需要回落</para>
        /// </remarks>
        public string FallbackType { get; set; }
        /// <summary>回落数字短信的模板Cod</summary>
        public string DigitalTemplateCode { get; set; }
        /// <summary>预留给调用方使用的ID</summary>
        public string OutId { get; set; }
        /// <summary>回落文本短信的模板变量对应的实际值 {\"jifen\":\"回落参数\"}</summary>
        public string SmsTemplateParam { get; set; }
        /// <summary>回落数字短信的模板变量对应的实际值 {\"msg\",\"xxxd\"}</summary>
        public string DigitalTemplateParam { get; set; }
        /// <summary>文本短信模板CODE</summary>
        public string TemplateCode { get; set; }
        /// <summary>短信模板变量对应的实际值，JSON格式{ \"code\": \"1111\" }</summary>
        public string TemplateParam { get; set; }
    }
    public class CardObjects {
        /// <summary>渲染失败后跳转链接 https://alibaba.com</summary>
        public string customUrl { get; set; }
        /// <summary>动态参数。动参变量不需要${}</summary>
        /// <remarks>{\"param3\":\"李四3\",\"param1\":\"李四\",\"param2\":\"李四2\"}</remarks>
        public string dyncParams { get; set; }
        /// <summary>手机号码</summary>
        public string mobile { get; set; }
    }

    public class SendCardSmsResponse : AlibabaResponse
    {
        public SendCardSmsData Data { get; set; }
    }
    public class SendCardSmsData
    {
        /// <summary>发送卡片短信的手机号</summary>
        public string MediaMobiles { get; set; }
        /// <summary>卡片短信发送ID</summary>
        public string BizCardId { get; set; }
        /// <summary>数字短信发送ID</summary>
        public string BizDigitalId { get; set; }
        /// <summary>卡片短信模板审核状态</summary>
        /// <remarks>
        /// <para>0：审核中</para>
        /// <para>1：审核通过</para>
        /// <para>2：审核不通过</para>
        /// </remarks>
        public int CardTmpState { get; set; }
        /// <summary>回落的手机号</summary>
        public string NotMediaMobiles { get; set; }
        /// <summary>文本短信发送ID</summary>
        public string BizSmsId { get; set; }
    }
}
