﻿namespace EOH.Sms.Alibaba.Req
{
    /// <summary>获取OSS上传信息</summary>
    /// <remarks>获取卡片短信所属OSS资源配置信息，此配置信息将用于后续OSS文件上传操作</remarks>
    public class GetOSSInfoForCardTemplateRequest : AlibabaRequest<GetOSSInfoForCardTemplateResponse>
    {
        public GetOSSInfoForCardTemplateRequest()
        {
            Action = "GetOSSInfoForCardTemplate";
        }
    }
    public class GetOSSInfoForCardTemplateResponse : AlibabaResponse
    {
        /// <summary>返回数据</summary>
        public GetOSSInfoForCardTemplateData Data { get; set; }
    }
    public class GetOSSInfoForCardTemplateData {
        /// <summary>访问地址</summary>
        public string Signature { get; set; }
        /// <summary>访问地址</summary>
        public string Host { get; set; }
        /// <summary>签名策略</summary>
        public string Policy { get; set; }
        /// <summary>签名策略</summary>
        public string ExpireTime { get; set; }
        /// <summary>阿里云账号ID</summary>
        public string AliUid { get; set; }
        /// <summary>签名使用的AccessKey ID</summary>
        public string AccessKeyId { get; set; }
        /// <summary>策略路径</summary>
        public string StartPath { get; set; }
        /// <summary>OSS文件保存桶名称</summary>
        public string Bucket { get; set; }
    }
}
