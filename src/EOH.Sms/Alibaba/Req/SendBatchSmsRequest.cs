﻿using System.Collections.Generic;

namespace EOH.Sms.Alibaba.Req
{
    /// <summary>批量发送短信</summary>
    /// <remarks>批量发送短信，支持多个号码，多个签名，注意模板只能用一个</remarks>
    public class SendBatchSmsRequest : AlibabaRequest<SendBatchSmsModel, SendSmsResponse>
    {
        public SendBatchSmsRequest()
        {
            Action = "SendBatchSms";
        }
    }

    public class SendBatchSmsModel
    {
        /// <summary>接收短信的手机号码</summary>
        /// <remarks>手机号码格式： 
        /// <para>国内短信：+/+86/0086/86或无任何前缀的11位手机号码，例如1590000****。</para>
        /// <para>国际/港澳台消息：国际区号+号码，例如852000012****。</para>
        /// <para>支持对多个手机号码发送短信，手机号码之间以半角逗号（,）分隔。上限为1000个手机号码。批量调用相对于单条调用及时性稍有延迟。</para></remarks>
        public IList<string> PhoneNumberJson { get; set; }
        /// <summary>短信签名名称</summary>
        /// <remarks>必须是已添加、并通过审核的短信签名；且短信签名的个数必须与手机号码的个数相同、内容一一对应</remarks>
        public IList<string> SignNameJson { get; set; }
        /// <summary>短信模板CODE</summary>
        public string TemplateCode { get; set; }
        /// <summary>短信模板变量对应的实际值</summary>
        /// <remarks>如果JSON中需要带换行符，请参照标准的JSON协议处理；且模板变量值的个数必须与手机号码、签名的个数相同、内容一一对应，表示向指定手机号码中发对应签名的短信，且短信模板中的变量参数替换为对应的值</remarks>
        public IList<string> TemplateParamJson { get; set; }
        /// <summary>上行短信扩展码，JSON数组格式</summary>
        public string SmsUpExtendCodeJson { get; set; }
        /// <summary>外部流水扩展字段，长度小于256的字符串</summary>
        public string OutId { get; set; }
    }
}
