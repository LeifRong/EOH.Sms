﻿using System.Collections.Generic;

namespace EOH.Sms.Alibaba.Req
{
    /// <summary>批量发送卡片短信</summary>
    public class SendBatchCardSmsRequest : AlibabaRequest<SendBatchCardSmsModel, SendCardSmsResponse>
    {
        public SendBatchCardSmsRequest()
        {
            Action = "SendBatchCardSms";
        }
    }
    public class SendBatchCardSmsModel
    {
        /// <summary>卡片短信模板Code</summary>
        public string CardTemplateCode { get; set; }
        /// <summary>回落文本短信的模板Code</summary>
        public string SmsTemplateCode { get; set; }
        /// <summary>回落类型</summary>
        /// <remarks>
        /// <para>SMS：不支持卡片短信的号码，回落文本短</para>
        /// <para>DIGITALSMS：不支持卡片短信的号码，回落数字短信</para>
        /// <para>NONE：不需要回落</para>
        /// </remarks>
        public string FallbackType { get; set; }
        /// <summary>回落数字短信的模板Code</summary>
        public string DigitalTemplateCode { get; set; }
        /// <summary>预留给调用方使用的ID</summary>
        public string OutId { get; set; }
        /// <summary>接收短信的手机号码</summary>
        public IList<string> PhoneNumberJson { get; set; }
        /// <summary>短信签名名称</summary>
        public IList<string> SignNameJson { get; set; }
        /// <summary>卡片短信模板参数对应的实际值</summary>
        /// <remarks>[{"a":1,"b":2},{"a":9,"b":8}]</remarks>
        public string CardTemplateParamJson { get; set; }
        /// <summary>回落文本短信的模板变量对应的实际值</summary>
        /// <remarks>[{\"customurl\":\"http://www.alibaba.com\",\"dyncParams\":\"{\\\"a\\\":\\\"hello\\\",\\\"b\\\":\\\"world\\\"}\"}]</remarks>
        public string SmsTemplateParamJson { get; set; }
        /// <summary>回落数字短信的模板变量对应的实际值</summary>
        /// <remarks>[{"a":1,"b":2},{"a":9,"b":8}]</remarks>
        public string DigitalTemplateParamJson { get; set; }
        /// <summary>上行短信扩展码</summary>
        /// <remarks>[\"6\",\"6\"]</remarks>
        public string SmsUpExtendCodeJson { get; set; }
        /// <summary>文本短信模板CODE</summary>
        public string TemplateCode { get; set; }
        /// <summary>短信模板变量对应的实际值，JSON格式</summary>
        /// <remarks>[{"name":"TemplateParamJson"},{"name":"TemplateParamJson"}]</remarks>
        public string TemplateParamJson { get; set; }
    }
}
