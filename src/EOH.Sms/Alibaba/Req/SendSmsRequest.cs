﻿using System.Collections.Generic;

namespace EOH.Sms.Alibaba.Req
{
    /// <summary>发送短信</summary>
    /// <remarks>发送前请申请短信签名和短信模板，并确保签名和模板已审核通过</remarks>
    public class SendSmsRequest : AlibabaRequest<SendSmsModel, SendSmsResponse>
    {
        public SendSmsRequest()
        {
            Action = "SendSms";
        }
    }
    public class SendSmsModel
    {
        /// <summary>接收短信的手机号码</summary>
        /// <remarks>手机号码格式： 
        /// <para>国内短信：+/+86/0086/86或无任何前缀的11位手机号码，例如1590000****。</para>
        /// <para>国际/港澳台消息：国际区号+号码，例如852000012****。</para>
        /// <para>支持对多个手机号码发送短信，手机号码之间以半角逗号（,）分隔。上限为1000个手机号码。批量调用相对于单条调用及时性稍有延迟。</para></remarks>
        public IList<string> PhoneNumbers { get; set; }
        /// <summary>短信签名名称</summary>
        public string SignName { get; set; }
        /// <summary>短信模板CODE</summary>
        public string TemplateCode { get; set; }
        /// <summary>短信模板变量对应的实际值</summary>
        public string TemplateParam { get; set; }
        /// <summary>上行短信扩展码，JSON数组格式</summary>
        public string SmsUpExtendCodeJson { get; set; }
        /// <summary>外部流水扩展字段，长度小于256的字符串</summary>
        public string OutId { get; set; }
    }
    public class SendSmsResponse : AlibabaResponse
    {
        /// <summary>发送回执ID</summary>
        public string BizId { get; set; }
    }
}
