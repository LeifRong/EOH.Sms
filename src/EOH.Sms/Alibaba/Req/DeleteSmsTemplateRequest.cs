﻿namespace EOH.Sms.Alibaba.Req
{
    /// <summary>删除短信模板</summary>
    public class DeleteSmsTemplateRequest : AlibabaRequest<DeleteSmsTemplateModel, AddSmsSignResponseResponse>
    {
        public DeleteSmsTemplateRequest()
        {
            Action = "DeleteSmsTemplate";
        }
    }
    public class DeleteSmsTemplateModel
    {
        /// <summary>短信模板Code</summary>
        public string TemplateCode { get; set; }
    }
}
