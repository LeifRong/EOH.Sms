﻿namespace EOH.Sms.Alibaba.Req
{
    /// <summary>创建短链</summary>
    public class AddShortUrlRequest : AlibabaRequest<AddShortUrlModel, AddShortUrlResponse>
    {
        public AddShortUrlRequest()
        {
            Action = "AddShortUrl";
        }
    }
    public class AddShortUrlModel
    {
        /// <summary>原始链接地址。不超过1000个字符</summary>
        public string SourceUrl { get; set; }
        /// <summary>短链服务名称。不超过13个字符</summary>
        public string ShortUrlName { get; set; }
        /// <summary>短链服务使用有效期 默认7天。单位为天，有效期最长为90天</summary>
        public string EffectiveDays { get; set; } = "7";
    }
    public class AddShortUrlResponse : AlibabaResponse
    {
        /// <summary>短链详情</summary>
        public AddShortUrlData Data { get; set; }
    }

    public class AddShortUrlData {
        /// <summary>原始链接地址。不超过1000个字符</summary>
        public string SourceUrl { get; set; }
        /// <summary>生成的短链服务地址。可通过AddShortUrl接口获取</summary>
        public string ShortUrl { get; set; }
        /// <summary>短链服务使用失效时间</summary>
        public string ExpireDate { get; set; }
    }
}
