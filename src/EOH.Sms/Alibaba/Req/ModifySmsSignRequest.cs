﻿namespace EOH.Sms.Alibaba.Req
{
    /// <summary>修改短信签名</summary>
    /// <remarks>修改短信签名，并重新提交审核，审核中的签名不支持修改</remarks>
    public class ModifySmsSignRequest : AlibabaRequest<AddSmsSignModel, AddSmsSignResponse>
    {
        public ModifySmsSignRequest()
        {
            Action = "ModifySmsSign";
        }
    }
}
