﻿using System.Collections.Generic;

namespace EOH.Sms.Alibaba.Req
{
    /// <summary>删除模板标签</summary>
    public class UntagResourcesRequest : AlibabaRequest<UntagResourcesModel, AlibabaResponse>
    {
        public UntagResourcesRequest()
        {
            Action = "UntagResources";
        }
    }
    public class UntagResourcesModel
    {
        // /// <summary>资源类型，默认取值：TEMPLATE</summary>
        public string ResourceType { get; set; } = "TEMPLATE";
        /// <summary>地域ID，默认取值：cn-hangzhou</summary>
        public string RegionId { get; set; } = "cn-hangzhou";
        /// <summary>是否删除该模板下的所有标签</summary>
        public bool All { get; set; }
        /// <summary>产品名。默认取值：dysms</summary>
        public string ProdCode { get; set; } = "dysms";
        /// <summary>标签键。标签键的数量范围为[1, 20]</summary>
        public IList<string> TagKey { get; set; }
        /// <summary>短信模板Code</summary>
        public IList<string> ResourceId { get; set; }
    }
}
