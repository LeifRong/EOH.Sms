﻿using System.Collections.Generic;

namespace EOH.Sms.Alibaba.Req
{
    /// <summary>查询短信发送统计信息</summary>
    public class QuerySendStatisticsRequest : AlibabaRequest<QuerySendStatisticsModel, QuerySendStatisticsResponse>
    {
        public QuerySendStatisticsRequest()
        {
            Action = "QuerySendStatistics";
        }
    }

    public class QuerySendStatisticsModel
    {
        /// <summary>短信发送范围，默认1。
        /// <para>1：国内短信发送记录。</para>
        /// <para>2：国际/港澳台短信发送记录</para></summary>
        public int IsGlobe { get; set; } = 1;
        /// <summary>开始日期，格式为yyyyMMdd，例如20181225</summary>
        public string StartDate { get; set; }
        /// <summary>结束日期，格式为yyyyMMdd，例如20181225</summary>
        public string EndDate { get; set; }
        /// <summary>当前页码。默认取值为1</summary>
        public int PageIndex { get; set; } = 1;
        /// <summary>每页显示的条数。默认取值为10 取值范围：1~50</summary>
        public int PageSize { get; set; } = 10;
        /// <summary>模板类型。默认取值为0
        /// <para>0：验证码。</para>
        /// <para>1：通知短信。</para>
        /// <para>2：推广短信。（仅支持企业客户）</para>
        /// <para>3：国际/港澳台消息。（仅支持企业客户）</para>
        /// <para>7：数字短信</para></summary>
        public int TemplateType { get; set; } = 0;
        /// <summary>签名名称</summary>
        public string SignName { get; set; }
    }
    public class QuerySendStatisticsResponse : AlibabaResponse
    {
        /// <summary>返回数据</summary>
        public QuerySendStatisticsData Data { get; set; }
    }
    public class QuerySendStatisticsData
    {
        /// <summary>返回数据的总条数</summary>
        public long TotalSize { get; set; }
        /// <summary>返回数据列表</summary>
        public IList<QuerySendStatisticsList> TargetList { get; set; }
    }
    public class QuerySendStatisticsList
    {
        /// <summary>发送成功的短信条数</summary>
        public long TotalCount { get; set; }
        /// <summary>接收到回执成功的短信条数</summary>
        public long RespondedSuccessCount { get; set; }
        /// <summary>接收到回执失败的短信条数</summary>
        public long RespondedFailCount { get; set; }
        /// <summary>未收到回执的短信条数</summary>
        public long NoRespondedCount { get; set; }
        /// <summary>短信发送日期，格式为yyyyMMdd，例如20181225</summary>
        public string SendDate { get; set; }
    }
}
