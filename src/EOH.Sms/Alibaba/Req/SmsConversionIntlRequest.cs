﻿namespace EOH.Sms.Alibaba.Req
{
    /// <summary>国内发国际短信转化反馈</summary>
    /// <remarks>将每一条消息ID(MessageId) 对应短信的接收情况反馈给阿里云国际短信平台</remarks>
    public class SmsConversionIntlRequest : AlibabaRequest<SmsConversionIntlModel, AlibabaResponse>
    {
        public SmsConversionIntlRequest()
        {
            Action = "SmsConversionIntl";
        }
    }
    public class SmsConversionIntlModel
    {
        /// <summary>消息ID</summary>
        public string MessageId { get; set; }
        /// <summary>如果您的用户回复了您发送的消息，则设置为 true。否则，设置为 false符</summary>
        public bool Delivered { get; set; } = true;
        /// <summary>触达发送目标的时间戳。必须是Unix时间戳，毫秒级别长整型</summary>
        /// <remarks>
        /// <para>如果不指定该字段：默认当前的时间戳</para>
        /// <para>如果指定该字段：该时间戳必须大于发送时间并且小于当前时间戳</para>
        /// </remarks>
        public long ConversionTime { get; set; }
    }
}
