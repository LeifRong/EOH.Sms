﻿using System.Collections.Generic;

namespace EOH.Sms.Alibaba.Req
{
    /// <summary>查询模板标签</summary>
    public class ListTagResourcesRequest : AlibabaRequest<ListTagResourcesModel, AlibabaResponse>
    {
        public ListTagResourcesRequest()
        {
            Action = "ListTagResources";
        }
    }
    public class ListTagResourcesModel : TagResourcesModel
    {
        /// <summary>查询下一页标签的Token</summary>
        public string NextToken { get; set; }
        /// <summary>每页显示条数, 默认20</summary>
        public int PageSize { get; set; } = 20;
    }
    public class ListTagResourcesResponse : AlibabaResponse
    {
        /// <summary>标签资源</summary>
        public IList<TagResources> TagResources { get; set; }
    }
    public class TagResources
    {
        /// <summary>资源类型 ALIYUN::DYSMS::TEMPLAT</summary>
        public string ResourceType { get; set; }
        /// <summary>标签值</summary>
        public string TagValue { get; set; }
        /// <summary>短信模板Code</summary>
        public string ResourceId { get; set; }
        /// <summary>标签键</summary>
        public string TagKey { get; set; }
    }
}
