﻿namespace EOH.Sms.Alibaba.Req
{
    /// <summary>获取媒体资源ID</summary>
    /// <remarks>将用户上传到卡片短信OSS存储的图片、视频转换成（生成）资源数据统一管理，并返回资源ID，用户可以对返回的资源ID自行管理</remarks>
    public class GetMediaResourceIdRequest : AlibabaRequest<GetMediaResourceIdModel, AlibabaResponse>
    {
        public GetMediaResourceIdRequest()
        {
            Action = "GetMediaResourceId";
        }
    }
    public class GetMediaResourceIdModel
    {
        /// <summary>资源类型, 默认1</summary>
        /// <remarks>
        /// <para>1：文本</para>
        /// <para>2：图片。小图片限制在100 KB以内，大图片限制在2 MB以内，图片要求清晰；图片格式支持JPG、JPEG、PNG</para>
        /// <para>3：音频</para>
        /// <para>4：视频。视频格式支持MP4</para>
        /// </remarks>
        public int ResourceType { get; set; } = 1;
        /// <summary>获取的资源地址</summary>
        public string OssKey { get; set; }
        /// <summary>扩展字段</summary>
        public long FileSize { get; set; }
        /// <summary>扩展字段</summary>
        public string ExtendInfo { get; set; }
        /// <summary>上传资源的描述</summary>
        public string Memo { get; set; }
    }
    public class GetMediaResourceIdResponse : AlibabaResponse
    {
        /// <summary>返回数据</summary>
        public GetMediaResourceIdData Data { get; set; }
    }
    public class GetMediaResourceIdData
    {
        /// <summary>资源ID</summary>
        public string ResourceId { get; set; }
        /// <summary>资源下载地址</summary>
        public string ResUrlDownload { get; set; }
    }
}
