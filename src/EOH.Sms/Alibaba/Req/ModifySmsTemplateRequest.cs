﻿namespace EOH.Sms.Alibaba.Req
{
    /// <summary>修改审核未通过的短信模板</summary>
    /// <remarks>修改未审核通过的短信模板信息，并重新提交审核</remarks>
    public class ModifySmsTemplateRequest : AlibabaRequest<ModifySmsTemplateModel, AddSmsSignResponse>
    {
        public ModifySmsTemplateRequest()
        {
            Action = "ModifySmsTemplate";
        }
    }
    public class ModifySmsTemplateModel : AddSmsTemplateModel
    {
        /// <summary>短信模板Code</summary>
        public string TemplateCode { get; set; }
    }
}
