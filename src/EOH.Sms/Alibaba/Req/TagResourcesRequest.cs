﻿using System.Collections.Generic;

namespace EOH.Sms.Alibaba.Req
{
    /// <summary>添加模板标签</summary>
    public class TagResourcesRequest : AlibabaRequest<TagResourcesModel, AlibabaResponse>
    {
        public TagResourcesRequest()
        {
            Action = "TagResources";
        }
    }
    public class TagResourcesModel
    {
        /// <summary>资源类型，默认取值：TEMPLATE</summary>
        public string ResourceType { get; set; } = "TEMPLATE";
        ///// <summary>地域ID，默认取值：cn-hangzhou</summary>
        //public string RegionId { get; set; } = "cn-hangzhou";
        /// <summary>产品名。默认取值：dysms</summary>
        public string ProdCode { get; set; } = "dysms";
        /// <summary>标签</summary>
        public IList<Tag> Tag { get; set; }
        /// <summary>短信模板Code</summary>
        public IList<string> ResourceId { get; set; }
    }
    public class Tag {
        /// <summary>标签键。N的取值范围为[1, 20]</summary>
        public string Key { get; set; }
        /// <summary>标签值。N的取值范围为[1, 20]</summary>
        public string Value { get; set; }
    }
}
