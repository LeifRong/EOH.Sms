﻿using System.Collections.Generic;

namespace EOH.Sms.Alibaba.Req
{
    /// <summary>查询短信模板列表</summary>
    public class QuerySmsTemplateListRequest : AlibabaRequest<QuerySmsSignListModel, QuerySmsTemplateListResponse>
    {
        public QuerySmsTemplateListRequest()
        {
            Action = "QuerySmsTemplateList";
        }
    }
    public class QuerySmsTemplateListResponse : AlibabaResponse
    {
        /// <summary>结果列表</summary>
        public IList<SmsTemplate> SmsTemplateList { get; set; }
        /// <summary>签名总数</summary>
        public long TotalCount { get; set; }
        /// <summary>页码。默认取值为1</summary>
        public int CurrentPage { get; set; } = 1;
        /// <summary>每页展示的签名条数。取值范围：1~50默认取值为10</summary>
        public int PageSize { get; set; } = 10;
    }

    public class SmsTemplate : AddSmsTemplateModel {
        /// <summary>模板类型（推荐对外使用）</summary>
        /// <remarks>
        /// <para>0：验证码</para>
        /// <para>1：短信通知</para>
        /// <para>2：推广短信</para>
        /// <para>3：国际/港澳台消息</para>
        /// <para>7：数字短信</para>
        /// </remarks>
        public int OuterTemplateType { get; set; } = 1;
        /// <summary>签名审批状态</summary>
        /// <remarks>
        /// <para>AUDIT_STATE_INIT：审核中</para>
        /// <para>AUDIT_STATE_PASS：审核通过</para>
        /// <para>AUDIT_STATE_NOT_PASS：审核未通过，请在返回参数Reason中查看审核未通过原因</para>
        /// <para>AUDIT_STATE_CANCEL：取消审核</para>
        /// </remarks>
        public string AuditStatus { get; set; }
        /// <summary>短信签名的创建日期和时间，格式为yyyy-MM-dd HH:mm:ss</summary>
        public string CreateDate { get; set; }
        /// <summary>审核备注</summary>
        public Reason Reason { get; set; }
    }
}
