﻿using System.Collections.Generic;

namespace EOH.Sms.Alibaba.Req
{
    /// <summary>查询短信签名列表</summary>
    public class QuerySmsSignListRequest : AlibabaRequest<QuerySmsSignListModel, QuerySmsSignListResponse>
    {
        public QuerySmsSignListRequest()
        {
            Action = "QuerySmsSignList";
        }
    }
    public class QuerySmsSignListModel
    {
        /// <summary>展示第几页的签名信息。默认取值为1</summary>
        public int PageIndex { get; set; } = 1;
        /// <summary>每页展示的签名条数。取值范围：1~50 默认取值为10</summary>
        public int PageSize { get; set; } = 10;
    }
    public class QuerySmsSignListResponse : AlibabaResponse
    {
        public IList<SmsSign> SmsSignList { get; set; }
        /// <summary>签名总数</summary>
        public long TotalCount { get; set; }
        /// <summary>页码。默认取值为1</summary>
        public int CurrentPage { get; set; } = 1;
        /// <summary>每页展示的签名条数。取值范围：1~50默认取值为10</summary>
        public int PageSize { get; set; } = 10;
    }

    public class SmsSign
    {
        /// <summary>签名名称</summary>
        public string SignName { get; set; }
        /// <summary>签名审批状态</summary>
        /// <remarks>
        /// <para>AUDIT_STATE_INIT：审核中</para>
        /// <para>AUDIT_STATE_PASS：审核通过</para>
        /// <para>AUDIT_STATE_NOT_PASS：审核未通过，请在返回参数Reason中查看审核未通过原因</para>
        /// <para>AUDIT_STATE_CANCEL：取消审核</para>
        /// </remarks>
        public string AuditStatus { get; set; }
        /// <summary>短信签名的创建日期和时间，格式为yyyy-MM-dd HH:mm:ss</summary>
        public string CreateDate { get; set; }
        /// <summary>审核备注</summary>
        public Reason Reason { get; set; }
        /// <summary>签名场景类型。返回值以”类型“结尾</summary>
        /// <remarks>
        /// 取值
        /// <para>验证码类型</para>
        /// <para>通用类型</para>
        /// </remarks>
        public string BusinessType { get; set; }
        /// <summary>工单ID</summary>
        public string OrderId { get; set; }
    }

    public class Reason {
        /// <summary>审批未通过的备注信息</summary>
        public string RejectSubInfo { get; set; }
        /// <summary>审批未通过的时间，格式为yyyy-MM-dd HH:mm:ss</summary>
        public string RejectDate { get; set; }
        /// <summary>审批未通过的原因</summary>
        public string RejectInfo { get; set; }
    }
}
