﻿namespace EOH.Sms.Alibaba.Req
{
    /// <summary>删除短链</summary>
    /// <remarks>删除短链，删除后短链将无法使用，无法还原为原链</remarks>
    public class DeleteShortUrlRequest : AlibabaRequest<DeleteShortUrlModel, AlibabaResponse>
    {
        public DeleteShortUrlRequest()
        {
            Action = "DeleteShortUrl";
        }
    }
    public class DeleteShortUrlModel
    {
        /// <summary>原始链接地址。不超过1000个字符</summary>
        public string SourceUrl { get; set; }
    }
}
