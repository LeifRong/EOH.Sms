﻿using System.Collections.Generic;

namespace EOH.Sms.Alibaba.Req
{
    /// <summary>查询短信发送详情</summary>
    public class QuerySendDetailsRequest : AlibabaRequest<QuerySendDetailsModel, QuerySendDetailsResponse>
    {
        public QuerySendDetailsRequest()
        {
            Action = "QuerySendDetails";
        }
    }

    public class QuerySendDetailsModel
    {
        /// <summary>接收短信的手机号码</summary>
        /// <remarks>手机号码格式： 
        /// <para>国内短信：+/+86/0086/86或无任何前缀的11位手机号码，例如1590000****。</para>
        /// <para>国际/港澳台消息：国际区号+号码，例如852000012****。</para>
        /// <para>支持对多个手机号码发送短信，手机号码之间以半角逗号（,）分隔。上限为1000个手机号码。批量调用相对于单条调用及时性稍有延迟。</para></remarks>
        public string PhoneNumber { get; set; }
        /// <summary>发送回执ID，即发送流水号。调用发送接口SendSms或SendBatchSms发送短信时，返回值中的BizId字段</summary>
        public string BizId { get; set; }
        /// <summary>短信发送日期，支持查询最近30天的记录。格式为yyyyMMdd，例如20181225</summary>
        public string SendDate { get; set; }
        /// <summary>当前页码。默认取值为1</summary>
        public long CurrentPage { get; set; } = 1;
        /// <summary>每页显示的条数。默认取值为10 取值范围：1~50</summary>
        public long PageSize { get; set; } = 10;
    }
    public class QuerySendDetailsResponse : AlibabaResponse
    {
        /// <summary>短信发送总条数</summary>
        public long TotalCount { get; set; }
        /// <summary>短信发送明细</summary>
        public QuerySendDetailsSmsSendDetailDTOs SmsSendDetailDTOs { get; set; }
    }
    public class QuerySendDetailsSmsSendDetailDTOs {
        /// <summary>短信发送明细</summary>
        public IList<QuerySendDetailsData> SmsSendDetailDTO { get; set; }
    }
    public class QuerySendDetailsData
    {
        /// <summary>运营商短信状态码。 短信发送成功：DELIVERED。 短信发送失败：失败错误码请参见错误码。</summary>
        public string ErrCode { get; set; }
        /// <summary>短信模板ID</summary>
        public string TemplateCode { get; set; }
        /// <summary>外部流水扩展字段</summary>
        public string OutId { get; set; }
        /// <summary>短信接收日期和时间</summary>
        public string ReceiveDate { get; set; }
        /// <summary>短信发送日期和时间</summary>
        public string SendDate { get; set; }
        /// <summary>接收短信的手机号码</summary>
        public string PhoneNum { get; set; }
        /// <summary>短信内容</summary>
        public string Content { get; set; }
        /// <summary>短信发送状态</summary>
        /// <remarks><para>1：等待回执。</para>
        /// <para>2：发送失败。</para>
        /// <para>3：发送成功</para></remarks>
        public long SendStatus { get; set; }
    }
}
