﻿using System.Collections.Generic;

namespace EOH.Sms.Alibaba.Req
{
    /// <summary>获取卡片短信短链</summary>
    public class GetCardSmsLinkRequest : AlibabaRequest<GetCardSmsLinkModel, GetCardSmsLinkResponse>
    {
        public GetCardSmsLinkRequest()
        {
            Action = "GetCardSmsLink";
        }
    }
    public class GetCardSmsLinkModel
    {
        /// <summary>卡片短信模板Code</summary>
        public string CardTemplateCode { get; set; }
        /// <summary>外部流水扩展字段</summary>
        public string OutId { get; set; }
        /// <summary>外部流水扩展字段</summary>
        public IList<string> PhoneNumberJson { get; set; }
        /// <summary>短信签名名称</summary>
        public IList<string> SignNameJson { get; set; }
        /// <summary>卡片短信动参</summary>
        public string CardTemplateParamJson { get; set; }
        /// <summary>卡片短信短链编码类型, 默认1</summary>
        /// <remarks>
        /// <para>1：群发</para>
        /// <para>2：个性化</para>
        /// </remarks>
        public string CardCodeType { get; set; }
        /// <summary>卡片短信短链类型, 默认1</summary>
        /// <remarks>
        /// <para>1：标准生成短码</para>
        /// <para>2：自定义生成短码</para>
        /// </remarks>
        public int CardLinkType { get; set; } = 1;
        /// <summary>发送账号分配的短链域名，需要提前报备</summary>
        public string Domain { get; set; }
        /// <summary>客户自定义短码。长度为4~8位的数字或字母</summary>
        public string CustomShortCodeJson { get; set; }
    }

    public class GetCardSmsLinkResponse : AlibabaResponse
    {
        public GetCardSmsLinkData Data { get; set; }
    }
    public class GetCardSmsLinkData
    {
        /// <summary>卡片短信模板审核状态</summary>
        /// <remarks>
        /// <para>0：审核中</para>
        /// <para>1：审核通过</para>
        /// <para>2：审核不通过</para>
        /// </remarks>
        public int CardTmpState { get; set; }
        /// <summary>不支持卡片短信的手机号</summary>
        public string NotMediaMobiles { get; set; }
        /// <summary>支持卡片短信的手机号码</summary>
        public List<string> CardPhoneNumbers { get; set; }
        /// <summary>卡片短信短链</summary>
        public List<string> CardSmsLinks { get; set; }
        /// <summary>用于申请卡片短信短链的短信签名，在发送时签名、接收号码、卡片短信短链要一一对应</summary>
        public IList<string> CardSignNames { get; set; }
    }
}
