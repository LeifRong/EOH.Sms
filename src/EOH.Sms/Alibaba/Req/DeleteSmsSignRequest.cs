﻿namespace EOH.Sms.Alibaba.Req
{
    /// <summary>删除短信签名</summary>
    /// <remarks>删除签名，删除后您将不能使用它继续发短信。</remarks>
    public class DeleteSmsSignRequest : AlibabaRequest<DeleteSmsSignModel, AddSmsSignResponse>
    {
        public DeleteSmsSignRequest()
        {
            Action = "DeleteSmsSign";
        }
    }
    public class DeleteSmsSignModel
    {
        /// <summary>签名名称</summary>
        public string SignName { get; set; }
    }
}
