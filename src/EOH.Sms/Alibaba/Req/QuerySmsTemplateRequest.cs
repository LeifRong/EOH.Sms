﻿namespace EOH.Sms.Alibaba.Req
{
    /// <summary>查询短信模板的审核状态</summary>
    public class QuerySmsTemplateRequest : AlibabaRequest<DeleteSmsTemplateModel, QuerySmsTemplateResponse>
    {
        public QuerySmsTemplateRequest()
        {
            Action = "QuerySmsTemplate";
        }
    }
    public class QuerySmsTemplateResponse : AlibabaResponse
    {
        /// <summary>短信类型, 默认1</summary>
        /// <remarks>
        /// <para>0：验证码</para>
        /// <para>1：短信通知</para>
        /// <para>2：推广短信</para>
        /// <para>3：国际/港澳台消息</para>
        /// </remarks>
        public int TemplateType { get; set; } = 1;
        /// <summary>短信模板Code</summary>
        public string TemplateCode { get; set; }
        /// <summary>模板名称，长度不超过30个字符</summary>
        public string TemplateName { get; set; }
        /// <summary>模板内容，长度不超过500个字符</summary>
        /// <remarks>您正在申请手机注册，验证码为：${code}，5分钟内有效！</remarks>
        public string TemplateContent { get; set; }
        /// <summary>审核备注</summary>
        /// <remarks>
        /// <para>如果审核状态为审核通过或审核中，参数Reason显示为“无审核备注</para>
        /// <para>如果审核状态为审核未通过，参数Reason显示审核的具体原因</para>
        /// </remarks>
        public string Reason { get; set; }
        /// <summary>短信模板的创建时间</summary>
        public string CreateDate { get; set; }
        /// <summary>模板审核状态</summary>
        /// <remarks>
        /// <para>0：审核中</para>
        /// <para>1：审核通过</para>
        /// <para>2：审核失败，请在返回参数Reason中查看审核失败原因</para>
        /// <para>10：取消审核</para>
        /// </remarks>
        public int TemplateStatus { get; set; }
    }
}
