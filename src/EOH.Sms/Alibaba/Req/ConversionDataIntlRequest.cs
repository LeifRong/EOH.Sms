﻿namespace EOH.Sms.Alibaba.Req
{
    /// <summary>国内发国际转化率数据接入API</summary>
    /// <remarks>将短信转化率统计数据反馈给阿里云短信平台</remarks>
    public class ConversionDataIntlRequest : AlibabaRequest<ConversionDataIntlModel, AlibabaResponse>
    {
        public ConversionDataIntlRequest()
        {
            Action = "ConversionDataIntl";
        }
    }
    public class ConversionDataIntlModel
    {
        /// <summary>转化率观测的时间点。必须是Unix时间戳，毫秒级别长整型</summary>
        /// <remarks>如果不指定该字段：默认当前的时间戳</remarks>
        public long ReportTime { get; set; }
        /// <summary>转化率监控回报值</summary>
        /// <remarks>该参数取值为double类型 ，区间是[0,1]</remarks>
        public string ConversionRate { get; set; }
    }
}
