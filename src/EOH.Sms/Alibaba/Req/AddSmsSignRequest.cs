﻿using System.Collections.Generic;

namespace EOH.Sms.Alibaba.Req
{
    /// <summary>申请短信签名</summary>
    public class AddSmsSignRequest : AlibabaRequest<AddSmsSignModel, AddSmsSignResponse>
    {
        public AddSmsSignRequest()
        {
            Action = "AddSmsSign";
        }
    }
    public class AddSmsSignModel
    {
        /// <summary>签名名称</summary>
        /// <remarks>
        /// <para>签名名称不区分大小写字母，如【Aliyun通信】和【aliyun通信】视为名称相同。</para>
        /// <para>当您的验证码签名和通用签名名称相同时，系统默认使用通用签名发送短信。</para>
        /// </remarks>
        public string SignName { get; set; }
        /// <summary>签名来源 默认1</summary>
        /// <remarks>取值
        /// <para>0：企事业单位的全称或简称</para>
        /// <para>1：工信部备案网站的全称或简称</para>
        /// <para>2：App应用的全称或简称</para>
        /// <para>3：公众号或小程序的全称或简称</para>
        /// <para>4：电商平台店铺名的全称或简称</para>
        /// <para>5：商标名的全称或简称</para>
        /// </remarks>
        public int SignSource { get; set; } = 1;
        /// <summary>短信签名申请说明，长度不超过200个字符。</summary>
        /// <remarks>
        /// 场景说明是签名审核的参考信息之一
        /// <para>请详细描述已上线业务的使用场景，并提供可以验证这些业务的网站链接、已备案域名地址、应用市场下载链接、公众号或小程序全称等信息。对于登录场景，还需提供测试账号密码。信息完善的申请说明会提高签名、模板的审核效率</para>
        /// </remarks>
        public string Remark { get; set; }
        /// <summary>签名文件</summary>
        public IList<SignFile> SignFileList { get; set; }
        /// <summary>签名类型 默认1</summary>
        /// <remarks>取值
        /// <para>0：验证码</para>
        /// <para>1：通用</para>
        /// </remarks>
        public int SignType { get; set; } = 1;
    }
    /// <summary>签名文件</summary>
    public class SignFile
    {
        /// <summary>签名文件</summary>
        public string FileContents { get; set; }
        /// <summary>签名文件</summary>
        public string FileSuffix { get; set; }
    }
    public class AddSmsSignResponse : AlibabaResponse
    {
        /// <summary>签名名称</summary>
        public string SignName { get; set; }
    }

}
