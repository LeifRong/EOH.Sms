﻿namespace EOH.Sms.Alibaba.Req
{
    /// <summary>短链状态查询</summary>
    public class QueryShortUrlRequest : AlibabaRequest<QueryShortUrlModel, QueryShortUrlResponse>
    {
        public QueryShortUrlRequest()
        {
            Action = "QueryShortUrl";
        }
    }
    public class QueryShortUrlModel
    {
        /// <summary>生成的短链服务地址。可通过AddShortUrl接口获取</summary>
        public string ShortUrl { get; set; }
    }

    public class QueryShortUrlResponse : AlibabaResponse
    {
        /// <summary>短链详情</summary>
        public QueryShortUrlData Data { get; set; }
    }

    public class QueryShortUrlData
    {
        /// <summary>短链使用的UV数据</summary>
        public string UniqueVisitorCount { get; set; }
        /// <summary>原始链接地址</summary>
        public string SourceUrl { get; set; }
        /// <summary>短链状态。取值：
        /// expired：失效。
        /// effective：有效。
        /// audit：审核中。
        /// reject：审核拒绝</summary>
        public string ShortUrlStatus { get; set; }
        /// <summary>短链使用的PV数据</summary>
        public string PageViewCount { get; set; }
        /// <summary>短链失效时间</summary>
        public string ExpireDate { get; set; }
        /// <summary>短链服务名称</summary>
        public string ShortUrlName { get; set; }
        /// <summary>短链创建时间</summary>
        public string CreateDate { get; set; }
        /// <summary>生成的短链服务地址</summary>
        public string ShortUrl { get; set; }
    }

}
