﻿namespace EOH.Sms.Alibaba.Req
{
    /// <summary>创建卡片短信模板</summary>
    public class CreateCardSmsTemplateRequest : AlibabaRequest<CreateCardSmsTemplateModel, CreateCardSmsTemplateResponse>
    {
        public CreateCardSmsTemplateRequest()
        {
            Action = "CreateCardSmsTemplate";
        }
    }
    public class CreateCardSmsTemplateModel
    {
        /// <summary>卡片短信模板名称</summary>
        public string TemplateName { get; set; }
        /// <summary>卡片短信模板名称</summary>
        /// <remarks>参考：https://help.aliyun.com/document_detail/435361.html?spm=a2c4g.434113.0.0.74191ed77Nd3Bm</remarks>
        public object Template { get; set; }
        /// <summary>对上传模板的描述</summary>
        public string Memo { get; set; }
        /// <summary>模板提交的厂商，该参数不填时，系统自动匹配模板支持的手机厂商</summary>
        /// <remarks>
        /// <para>HuaWei：表示华为厂商</para>
        /// <para>XiaoMi：表示小米厂商</para>
        /// <para>OPPO：表示OPPO厂商</para>
        /// <para>VIVO：表示VIVO厂商</para>
        /// <para>MEIZU：表示魅族厂商</para>
        /// </remarks>
        public string Factorys { get; set; }
    }
    public class CreateCardSmsTemplateResponse : AlibabaResponse
    {
        /// <summary>返回对象。</summary>
        public CreateCardSmsTemplateData Data { get; set; }
    }
    public class CreateCardSmsTemplateData
    {
        /// <summary>模板Code</summary>
        public string TemplateCode { get; set; }
    }
}
