﻿namespace EOH.Sms.Alibaba.Req
{
    /// <summary>查询短信签名申请状态</summary>
    public class QuerySmsSignRequest : AlibabaRequest<DeleteSmsSignModel, QuerySmsSignResponse>
    {
        public QuerySmsSignRequest()
        {
            Action = "QuerySmsSign";
        }
    }
    public class QuerySmsSignResponse : AlibabaResponse
    {
        /// <summary>签名审核状态</summary>
        /// <remarks>
        /// <para>0：审核中</para>
        /// <para>1：审核通过</para>
        /// <para>2：审核失败，请在返回参数Reason中查看审核失败原因</para>
        /// <para>10：取消审核</para>
        /// </remarks>
        public int SignStatus { get; set; }
        /// <summary>短信签名的创建日期和时间</summary>
        public string CreateDate { get; set; }
        /// <summary>审核备注</summary>
        /// <remarks>
        /// <para>如果审核状态为审核通过或审核中，参数Reason显示为“无审核备注</para>
        /// <para>如果审核状态为审核未通过，参数Reason显示审核的具体原因</para>
        /// </remarks>
        public string Reason { get; set; }
        /// <summary>签名名称</summary>
        public string SignName { get; set; }
    }

}
