﻿namespace EOH.Sms.Alibaba
{
    /// <summary>配置项</summary>
    public class AlibabaOptions
    {
        /// <summary>访问密钥ID</summary>
        public string AccessKeyId { get; set; }
        /// <summary>访问密钥</summary>
        public string AccessKeySecret { get; set; }
        /// <summary>指定接口返回数据的格式。可以选择 JSON 或者 XML。默认为 JSON</summary>
        public string Format { get; set; } = "JSON";
        /// <summary>签名算法版本。目前为固定值 1.0</summary>
        public string SignatureVersion => "1.0";
        /// <summary>签名方式。目前为固定值 HMAC-SHA1</summary>
        public string SignatureMethod => "HMAC-SHA1";
        /// <summary>短信服务Url地域参数</summary>
        public string RegionId { get; set; } = "cn-shenzhen";
        /// <summary>短信服务Url</summary>
        public string BaseAddressUrl { get; set; } = "https://dysmsapi.aliyuncs.com";
        /// <summary>接口版本，默认：2017-05-25</summary>
        public string Version { get; set; } = "2017-05-25";
    }
}
