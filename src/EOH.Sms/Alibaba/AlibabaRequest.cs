﻿using System.Net.Http;
using System.Text.Json;

namespace EOH.Sms.Alibaba
{
    public abstract class AlibabaRequest<TResponse> : IRequest<TResponse> {
        public string Action { get; set; }
        public HttpMethod Method { get; set; } = HttpMethod.Post;
        public virtual TResponse ConvertResponse(string content)
        => JsonSerializer.Deserialize<TResponse>(content, new JsonSerializerOptions { PropertyNameCaseInsensitive = true });
    }
    public abstract class AlibabaRequest<TModel, TResponse> : AlibabaRequest<TResponse>, IRequest<TModel, TResponse>
    {
        public TModel Model { get; set; }
    }
}
